/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function () {
  var toggler = document.querySelector('[data-drupal-selector="menu-main__toggle"]');
  var menu = document.querySelector('[data-drupal-selector="menu-main"]');

  function toggleMenu() {
    toggler.classList.toggle('menu-main__toggle--active');
    menu.classList.toggle('menu-main--active');
    return false;
  }

  toggler.addEventListener('click', toggleMenu);
})();